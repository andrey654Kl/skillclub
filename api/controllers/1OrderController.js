var orders = {
	1:{
		dishes: [1,2,3],
		userID: 1,
		totalTimeout: 550,
		status: 1
	},
	2:{
	    dishes: [1,2],
		userID: 2,
		totalTimeout: 550,
		status: 2
	},
	3:{
	    dishes: [1,3],
		userID: 3,
		totalTimeout: 550,
		status: 1
	},
};

var sockets = [];

var sendToSockets = function (event, data) {
sockets.forEach(function (socketId) {
try {
sails.sockets.emit(socketId, event, data);
} catch (err) {
}
});
};

//sails.socket.blast

function deleteOrder(orderID){
     delete orders[orderID];
}

module.exports = {
	index: function(req,res){
    var user = {
    	"id":2,
    };
    req.session.user = user;
	return res.json(user);
	},
	close:function(req,res){

		if(req.session.user){
		     var orderID = req.param("orderID");
		     var userID = req.session.user.id;
		     if(orders[orderID]["userID"]==userID && orders[orderID]["status"]==2){
                   deleteOrder(orderID);
                    return res.json(orders);
             }else{
  		            return res.badRequest("Вы не можете выполнить данное действие");
             }
        }
		     return res.badRequest("Необходимо авторизоваться");
	},
	test:function(req,res){
		// sendToSockets("setState", orders);
         res.render("socketTest");
	},

	sock: function(req,res){
     if (!req.isSocket) {
         return res.json({isSocket:false});
      }
	if (sockets.indexOf(req.socket.id) === -1) {
		sockets.push(req.socket.id);
	}
	return res.json({socketId:req.socket.id})
	},
    other:function(req,res){
		sendToSockets("setOrders", orders);
         // res.render("socketTest");
	},

}