/**
 * OrderController
 *
 * @description :: Server-side logic for managing orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 function sendSockets(){
 	Order.find({status: ["wait","complete"]},function(err,orders){
 	  	       
 	  	       sails.sockets.blast({orders:orders});///123
         });
 	
 }

var userId = 1;
function getUser(req){
	if(!req.session.user){
    	req.session.user = {
    		id: userId++
    	};
	}
	return req.session.user;

}


module.exports = {
    index1:function(req,res){
         // Order.find({status: ["wait","complete"]},function(err,orders){
         //          return res.render('socketTest',{});
         // });
        return res.render('socketTest',{});
    },
    addOrder:function(req,res){

    	var request = {
    		dishes:[2,6]
    	}
    	var user = getUser(req)''
    	Dishes.find({id:request.dishes},function(err,dishes){
             var dishedID = [];
             var totalTimeout = 0;
             dishes.forEach(function(dishe){
                  dishedID.push(dishe.id);
                  totalTimeout += parseInt(dishe.timeout);
             });
             var newOrder = {
	    		userID : user.id,
	    		dishes:dishedID,
	    		totalTimeout:totalTimeout,
	    		status:"wait"
	    	};

    		Order.create(newOrder,function(err,neworder){
	    		if(err){
	    			return res.serverError(err);
	    		}
        		sendSockets();
		        setTimeout(function(){
		               Order.update({id:neworder.id}, {status: "complete"}, function(err, order){
		               	      sendSockets();  
		               });

		        },totalTimeout);
	    		return res.ok("added");	
	    	});
    		
    	});
    
    },
    sendSockets:function(req,res){
         // Order.find({status: ["wait","complete"]},function(err,orders){
         //         return res.json(orders); ///123
         // });
        sendSockets();
        return res.ok('socketTest');
    },
	close:function(req,res){
		req.session.user = {id:2}
		if(!req.session.user){
			return res.badRequest("need auth");
		}
		     var orderID = req.param("orderID");
		     var userID = req.session.user.id;

		     Order.findOne(orderID, function(err, order){
                if(err){
                	return res.badRequest(err);
                }
                if(!order){
                	return res.notFound("нет такого заказа");
                }
		     	if  (order.userID != req.session.user.id){
		     		return res.forbidden("Вы не можете выполнить данное действие");
         
		     	} 
		     	if  (order.status != "complete"){
		     		return res.badRequest("Вы не можете выполнить данное действие");
         
		     	} 

		     	order.status = "close";
		     	order.save(function(err, savedOrder){

		     	});		
				return res.ok("closed");
		     			

		     })
	},
};

